# ml-project

1. **Project.ipynb** : *모델별로 Hyper parameters를 튜닝하여 prediction test한 source file*
2. **Project_for_submission.ipynb** : *Project.ipynb 파일을 제출 형식으로 정리*
3. **CV_for_project.ipynb** : *각 모델별로 Hyper parameters를 튜닝을 위해 CV를 수행한 soruce file (참고용)*
